import React, { useRef, useState } from "react";
import { Switch, Route, Link, useParams } from "react-router-dom";
import { Menu, MenuItem } from "@material-ui/core";

import styles from "./App.module.scss";
import Planner from "./pages/planner";
import PlannerEditor from "./pages/planner-editor";
import UserPage from "./components/user/UserPage";

function App() {
  const { userId } = useParams();
  const menuAnchor = useRef(null);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const closeMenu = () => setIsMenuOpen(false);

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <Menu
          anchorEl={menuAnchor.current}
          open={isMenuOpen}
          onClose={closeMenu}
        >
          <MenuItem>
            <Link to={`${userId}/profile`}>Profile</Link>
          </MenuItem>
          <MenuItem>
            <Link to={`${userId}/planner`}>Planner</Link>
          </MenuItem>
        </Menu>

        <Switch>
          <Route
            path="/:userId/planner/:date/:mealType"
            component={PlannerEditor}
          />
          <Route path="/:userId/planner" component={Planner} />
          <Route path="/:userId/profile" component={UserPage} />
          <Route
            path="*"
            exact={true}
            component={() => <div>Page not found</div>}
          />
        </Switch>
      </div>
    </div>
  );
}

export default App;
