import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { Button, Checkbox } from "@material-ui/core";

import { fetchNutrition } from "../../store/nutrition/actions";
import { saveNutritionPlan } from "../../store/planner/actions";
import { nutritionSelector } from "../../store/nutrition/selectors";
import Meal from "../../components/meal";

import styles from "./styles.module.scss";

export default () => {
  const histoty = useHistory();
  const { userId, date, mealType } = useParams();
  const nutrition = useSelector(nutritionSelector);
  const dispatch = useDispatch();
  const nutritionPlanData = localStorage.getItem("nutritionPlan") || "{}";
  const nutritionPlan = JSON.parse(nutritionPlanData);
  const storedNutritionSelection = nutritionPlan[date] || {};
  const storedDishSelection = storedNutritionSelection[mealType] || [];
  const selectedDishIds = storedDishSelection.reduce(
    (acc, curr) => ({ ...acc, [curr.id]: true }),
    {}
  );
  const [selectedDishes, setSelectedDishes] = useState(selectedDishIds);
  const [isChanged, setIsChanged] = useState(false);
  const handleSelectMeal = (mealId, isChecked) => {
    setSelectedDishes({ ...selectedDishes, [mealId]: isChecked });
    setIsChanged(true);
  };
  useEffect(() => {
    dispatch(fetchNutrition(userId));
  }, [dispatch, userId]);

  const handleSave = () => {
    const disheslList = [];

    for (const [dishId, isSelected] of Object.entries(selectedDishes)) {
      if (isSelected) {
        disheslList.push(nutrition.find(({ id }) => +id === +dishId));
      }
    }

    dispatch(
      saveNutritionPlan({ date, userId, dishes: disheslList, mealType })
    );

    histoty.push(`/${userId}/planner/${date}`);
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <span>Select {mealType} dishes</span>
        <Button
          onClick={handleSave}
          variant="contained"
          color="secondary"
          disabled={!isChanged}
        >
          Save
        </Button>
      </div>
      {nutrition.map((meal) => (
        <Meal
          data={meal}
          editable={false}
          headerControls={
            <Checkbox
              color="default"
              checked={selectedDishes[meal.id]}
              onChange={({ target }) =>
                handleSelectMeal(meal.id, target.checked)
              }
            />
          }
        />
      ))}
    </div>
  );
};
