import React, { useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import moment from "moment";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import EditIcon from "@material-ui/icons/Edit";
import { Button, Modal } from "@material-ui/core";
import { PieChart } from "react-minimal-pie-chart";

// import {
//   Chart,
//   PieSeries,
//   Title,
// } from "@devexpress/dx-react-chart-material-ui";
import { Animation } from "@devexpress/dx-react-chart";
import classNames from "classnames";

import { fetchNutritionPlan } from "../../store/planner/actions";
import {
  isPendingNutritionPlanSelector,
  nutritionPlanSelector,
} from "../../store/planner/selectors";
import Meal from "../../components/dish";

import styles from "./styles.module.scss";

const DEFAULT_NUTRITION = {};

const getFormattedDate = (date) => moment(date).format("YYYY-MM-DD");

const getMealEditorLink = (userId, selectedDate, mealType) =>
  `/${userId}/planner/${getFormattedDate(selectedDate)}/${mealType}`;

const calcCaloriesPerMeal = (meal) =>
  meal.reduce((acc, curr) => acc + curr.calories, 0);

const hasData = (nutrition) => {
  const { breakfast = [], dinner = [], supper = [] } = nutrition;
  return !!(breakfast.length || dinner.length || supper.length);
};

const calcCallories = (nutrition) => {
  const { breakfast = [], dinner = [], supper = [] } = nutrition;
  console.log({ nutrition });
  return [
    {
      title: "Breakfast",
      value: calcCaloriesPerMeal(breakfast),
      color: "#8bc34a",
    },
    {
      title: "Dinner",
      value: calcCaloriesPerMeal(dinner),
      color: "#2196f3",
    },
    {
      title: "supper",
      value: calcCaloriesPerMeal(supper),
      color: "#9c27b0",
    },
  ];
};

const calcCaloriesBallance = (caloriesDestribution, targetCalories) =>
  (100 * caloriesDestribution.reduce((acc, { value }) => acc + value, 0)) /
  targetCalories;

const Dishes = ({ userId, selectedDate, nutrition = {}, type }) =>
  nutrition[type] ? (
    nutrition[type].map((meal) => (
      <Meal
        data={meal}
        type={type}
        headerControls={
          <Link to={getMealEditorLink(userId, selectedDate, type)}>
            <EditIcon />
          </Link>
        }
      />
    ))
  ) : (
    <div className={styles.addDishesButtonWrapper}>
      <Button
        variant="contained"
        color="secondary"
        className={styles.addDishesButton}
        href={getMealEditorLink(userId, selectedDate, type)}
      >
        Add {type}
      </Button>
    </div>
  );

export default () => {
  const { userId } = useParams();
  const dispatch = useDispatch();
  const [selectedDate, setSelecteDate] = useState(new Date());
  const [isCalloriesBallanceOpen, setIsCalloriesBallanceOpen] = useState(false);

  const planner = useSelector(nutritionPlanSelector);
  const isPendingNutritionPlan = useSelector(isPendingNutritionPlanSelector);
  const nutrition = useMemo(
    () => planner[getFormattedDate(selectedDate)] || DEFAULT_NUTRITION,
    [planner, selectedDate]
  );

  const caloriesDistribution = useMemo(() => calcCallories(nutrition), [
    nutrition,
  ]);

  useEffect(() => {
    dispatch(
      fetchNutritionPlan({
        userId,
        date: getFormattedDate(selectedDate),
      })
    );
  }, [dispatch, selectedDate, userId]);

  if (isPendingNutritionPlan) {
    return null;
  }

  console.log(caloriesDistribution);

  return (
    <div className={styles.container}>
      <div className={styles.title}>Your nutrition plan</div>
      <div className={styles.datepickerWrapper}>
        {hasData(nutrition) && (
          <Button
            variant="contained"
            color="secondary"
            onClick={() => setIsCalloriesBallanceOpen(true)}
          >
            {`Calories ${calcCaloriesBallance(
              caloriesDistribution,
              nutrition.targetCalories
            ).toFixed(2)}%`}
          </Button>
        )}

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            className={styles.datePicker}
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            value={selectedDate}
            onChange={(date) => setSelecteDate(date)}
          />
        </MuiPickersUtilsProvider>
      </div>
      <Dishes
        nutrition={nutrition}
        type="breakfast"
        userId={userId}
        date={selectedDate}
      />
      <Dishes
        nutrition={nutrition}
        type="dinner"
        userId={userId}
        date={selectedDate}
      />
      <Dishes
        nutrition={nutrition}
        type="supper"
        userId={userId}
        date={selectedDate}
      />
      <Modal
        open={isCalloriesBallanceOpen}
        onClose={() => setIsCalloriesBallanceOpen(false)}
      >
        <div className={styles.modal}>
          <div className={styles.legend}>
            <div
              className={classNames(
                styles.legendLabel,
                styles.breakfastLegendLabel
              )}
            >
              Breakfast
            </div>
            <div
              className={classNames(
                styles.legendLabel,
                styles.dinnerLegendLabel
              )}
            >
              Dinner
            </div>
            <div
              className={classNames(
                styles.legendLabel,
                styles.supperLegendLabel
              )}
            >
              Supper
            </div>
          </div>

          <PieChart
            onClick={() => setIsCalloriesBallanceOpen(false)}
            data={caloriesDistribution}
          />
        </div>
      </Modal>
    </div>
  );
};
