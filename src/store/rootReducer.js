import { combineReducers } from "redux";

import userSlice from "./user/slice";
import planner from "./planner/slice";
import nutrition from "./nutrition/slice";

const rootReducer = combineReducers({
  user: userSlice.reducer,
  planner: planner.reducer,
  nutrition: nutrition.reducer,
});

export default rootReducer;
