import { createSlice } from "@reduxjs/toolkit";

import { fetchNutritionPlan } from "./actions";

const nutritionPlan = createSlice({
  name: "fetchNutritionPlan",
  initialState: { entities: {}, error: null, pending: false },
  reducers: {
    clearUserState() {
      return { entities: {}, pending: false, error: null };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchNutritionPlan.fulfilled, (state, { payload }) => {
      state.entities[payload.date] = payload.planForSelectedDate;
      state.pending = false;
      state.error = null;
    });
    builder.addCase(fetchNutritionPlan.pending, (state) => {
      state.pending = true;
      state.error = null;
    });
    builder.addCase(fetchNutritionPlan.rejected, (state) => {
      state.pending = false;
      state.error = "Something went wrong";
    });
  },
});

export default nutritionPlan;
