export const nutritionPlanSelector = ({ planner }) => planner.entities;
export const isPendingNutritionPlanSelector = ({ planner }) => planner.pending;
