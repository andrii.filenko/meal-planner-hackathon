import { createAsyncThunk } from "@reduxjs/toolkit";

const TARGET_CALORIES_MOCK_VALUE = 5000;

const fetchNutritionPlanAction = ({ userId, date }) =>
  new Promise((resolve) => {
    setTimeout(() => {
      const nutritionPlanData = localStorage.getItem("nutritionPlan") || "{}";
      const nutritionPlan = JSON.parse(nutritionPlanData);
      const planForSelectedDate = nutritionPlan[date] || {};

      resolve({
        date,
        planForSelectedDate: {
          ...planForSelectedDate,
          targetCalories: TARGET_CALORIES_MOCK_VALUE,
        },
      });
    }, 500);
  });

export const fetchNutritionPlan = createAsyncThunk(
  "fetchNutritionPlan",
  fetchNutritionPlanAction
);

const saveNutritionPlanAction = ({ userId, date, mealType, dishes }) =>
  new Promise((resolve) => {
    setTimeout(() => {
      const nutritionPlanData = localStorage.getItem("nutritionPlan") || "{}";
      const nutritionPlan = JSON.parse(nutritionPlanData);
      const planForSelectedDate = nutritionPlan[date] || {};
      const updatedPlan = {
        ...planForSelectedDate,
        [mealType]: dishes,
      };

      localStorage.setItem(
        "nutritionPlan",
        JSON.stringify({
          ...nutritionPlan,
          [date]: updatedPlan,
        })
      );
      resolve(updatedPlan);
    }, 500);
  });

export const saveNutritionPlan = createAsyncThunk(
  "saveNutritionPlan",
  saveNutritionPlanAction
);
