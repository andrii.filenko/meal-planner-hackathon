export const nutritionSelector = ({ nutrition }) => nutrition.entities;
export const isPendingNutritionSelector = ({ nutrition }) => nutrition.pending;
