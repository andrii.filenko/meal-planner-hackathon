import { createAsyncThunk } from "@reduxjs/toolkit";

const fetchNutritionAction = ({ userId, date }) =>
  new Promise((resolve) => {
    setTimeout(
      () =>
        resolve([
          {
            id: 1,
            title: "Meal 1",
            image:
              "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
            calories: 800,
          },
          {
            id: 2,
            title: "Meal 2",
            image:
              "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/salmon-with-kale-dinner-for-two-1581382241.jpg?crop=0.503xw:1.00xh;0.250xw,0&resize=640:*",
            calories: 1000,
          },
          {
            id: 3,
            title: "Meal 3",
            image:
              "https://www.onceuponachef.com/images/2019/07/Big-Italian-Salad.jpg",
            calories: 1500,
          },
        ]),
      500
    );
  });

export const fetchNutrition = createAsyncThunk(
  "fetchNutrition",
  fetchNutritionAction
);
