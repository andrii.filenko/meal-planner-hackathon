import { createSlice } from "@reduxjs/toolkit";

import { fetchNutrition } from "./actions";

const nutrition = createSlice({
  name: "nutrition",
  initialState: { entities: [], error: null, pending: false },
  reducers: {
    clearUserState() {
      return { entities: [], pending: false, error: null };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchNutrition.fulfilled, (state, { payload }) => {
      state.entities = payload;
      state.pending = false;
      state.error = null;
    });
    builder.addCase(fetchNutrition.pending, (state) => {
      state.pending = true;
      state.error = null;
    });
    builder.addCase(fetchNutrition.rejected, (state) => {
      state.pending = false;
      state.error = "Something went wrong";
    });
  },
});

export default nutrition;
