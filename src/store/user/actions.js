import { createAsyncThunk } from "@reduxjs/toolkit";

const fetchUserAction = (id) =>
  new Promise((resolve) => {
    setTimeout(() => ({ id, name: "John Smith" }), 500);
  });
export const fetchUser = createAsyncThunk("fetchUser", fetchUserAction);
