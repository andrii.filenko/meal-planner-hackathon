import { createSlice } from "@reduxjs/toolkit";

import { fetchUser } from "./actions";

const user = createSlice({
  name: "user",
  initialState: { data: {}, error: null, pending: false },
  reducers: {
    clearUserState() {
      return { entities: [], pending: false, error: null };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUser.fulfilled, (state, { payload }) => {
      state.data = payload;
      state.pending = false;
      state.error = null;
    });
    builder.addCase(fetchUser.pending, (state) => {
      state.pending = true;
      state.error = null;
    });
    builder.addCase(fetchUser.rejected, (state) => {
      state.pending = false;
      state.error = "Something went wrong";
    });
  },
});

export default user;
