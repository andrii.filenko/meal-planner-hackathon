export const userSelector = ({ user }) => user.data;
export const isPendingUserSelector = ({ user }) => user.pending;
