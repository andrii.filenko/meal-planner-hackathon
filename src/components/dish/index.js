import React from "react";
import Paper from "@material-ui/core/Paper";

import imagePlaceholder from "../../assets/image-placeholder.png";
import styles from "./styles.module.scss";

export default ({ data, type, headerControls }) => {
  return (
    <Paper className={styles.container}>
      <div className={styles.title}>
        {`${data?.title}${type ? ` - ${type}` : ""}`}
        {headerControls}
      </div>
      <div
        className={styles.preview}
        style={{ backgroundImage: `url(${data?.image})` }}
      >
        <img src={imagePlaceholder} alt="" />
      </div>
      {data?.calories ? (
        <div
          className={styles.description}
        >{`Calories: ${data?.calories}`}</div>
      ) : null}
    </Paper>
  );
};
