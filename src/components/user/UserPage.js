import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import { connect } from "react-redux";
import { fetchUser } from "../../store/user/actions";
import {
  isPendingUserSelector,
  userSelector,
} from "../../store/user/selectors";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import {
  InputLabel,
  FormControl,
  MenuItem,
  Select,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100vh",
  },
  paper: {
    flex: 1,
  },
  form: {
    maxWidth: "75%",
    margin: "auto",
  },
  formControl: {
    margin: theme.spacing(1),
    display: "flex",
  },
  title: {
    textAlign: "center",
    marginTop: "20px",
    fontSize: "26px",
  },
}));

const UserPage = ({ fetchUser }) => {
  const classes = useStyles();
  const [user, setUser] = useState({});
  const { userId } = useParams();
  useEffect(() => {
    fetchUser(userId || "new_user");
  }, [fetchUser, userId]);

  const handleChange = (event, field) => {
    setUser({ ...user, [field]: event.target.value });
  };

  const saveSettings = () => {
    const {
      height = "",
      weight = "",
      gender = "male",
      allergens = "",
      activityLevel = "",
      hateFood,
    } = user;
    alert(
      `height: ${height}, weight: ${weight}, gender: ${gender}, allergens: ${allergens}, activityLevel: ${activityLevel}`
    );
  };

  return (
    <React.Fragment>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Typography variant="h4" className={classes.title} gutterBottom>
            Tell us about yourself
          </Typography>
          <Grid container spacing={3} className={classes.form}>
            <Grid item xs={12}>
              <TextField
                required
                id="height"
                name="height"
                label="Height"
                fullWidth
                onChange={(e) => handleChange(e, "height")}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                id="weight"
                name="weight"
                label="Weight"
                fullWidth
                onChange={(e) => handleChange(e, "weight")}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl component="fieldset">
                <FormLabel component="legend">Gender</FormLabel>
                <RadioGroup
                  aria-label="gender"
                  name="gender1"
                  value={user.gender || "male"}
                  onChange={(e) => handleChange(e, "gender")}
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="Male"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">
                  Activity Level
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  value={user.activityLevel || ""}
                  onChange={(e) => handleChange(e, "activityLevel")}
                >
                  <MenuItem value={10}>Low</MenuItem>
                  <MenuItem value={20}>Medium</MenuItem>
                  <MenuItem value={30}>High</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Allergens</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  value={user.allergens || ""}
                  onChange={(e) => handleChange(e, "allergens")}
                >
                  <MenuItem value={10}>Nuts</MenuItem>
                  <MenuItem value={20}>Milk</MenuItem>
                  <MenuItem value={30}>Eggs</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="country"
                name="country"
                label="Food you hate"
                fullWidth
                onChange={(e) => handleChange(e, "hateFood")}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                href={`/${userId}/planner`}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => ({
  data: userSelector(state),
  pending: isPendingUserSelector(state),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchUser: (id) => dispatch(fetchUser(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
