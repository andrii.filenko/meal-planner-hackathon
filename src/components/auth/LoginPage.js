import React from 'react';
import OktaSignIn from '@okta/okta-signin-widget';

export default class LoginPage extends React.Component {
    constructor(){
        super();
        this.widget = new OktaSignIn({
            baseUrl: 'https://dev-8857206.okta.com',
            clientId: '0oa8zndgWjRkDbBv15d5',
            // redirectUri: 'http://localhost:3000',
            authParams: {
                responseType: 'id_token'
            }
        });
    }

    componentDidMount(){
        this.widget.renderEl({el:this.loginContainer},
            (response) => {
                this.setState({user: response.claims.email});
            },
            (err) => {
                console.log(err);
            }
        );
    }

    render(){
        return(
            <div ref={(div) => {this.loginContainer = div; }} />
        );
    }
}